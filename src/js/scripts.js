
let articles = [
    {
        "img_location": "../src/img/Copenhagen.png",
        "title": "Wonderful Copenhagen 2011",
        "date": "Posted: 23/1-2011",
        "text": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Nyhavn.png",
        "title": "Nordic Barista Cup 2011 in Copenhagen\n",
        "date": "Posted: 22/1-2011",
        "text": "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark.\n" +
            "Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page.\n",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"

    },
    {
        "img_location": "../src/img/Winners.png",
        "title": "2010 Winners: Sweden" ,
        "date": "Posted: 12/1-2011",
        "text": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here.",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Copenhagen.png",
        "title": "Wonderful Stockholm 2012",
        "date": "Posted: 23/1-2011",
        "text": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Nyhavn.png",
        "title": "Nordic Barista Cup 2012 in Stockholm\n",
        "date": "Posted: 22/1-2012",
        "text": "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark.\n" +
            "Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page.\n",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"

    },
    {
        "img_location": "../src/img/Winners.png",
        "title": "2012 Winners: Sweden" ,
        "date": "Posted: 12/1-2012",
        "text": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here.",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Copenhagen.png",
        "title": "Wonderful Oslo 2013",
        "date": "Posted: 23/1-2013",
        "text": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Nyhavn.png",
        "title": "Nordic Barista Cup 2011 in Oslo\n",
        "date": "Posted: 22/1-2011",
        "text": "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark.\n" +
            "Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page.\n",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"

    },
    {
        "img_location": "../src/img/Winners.png",
        "title": "2013 Winners: Norway" ,
        "date": "Posted: 12/1-2013",
        "text": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here.",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Copenhagen.png",
        "title": "Wonderful Finland 2020",
        "date": "Posted: 23/1-2020",
        "text": "The aim is to understand the science behind our sensory perceptions. And by stimulating the senses we will improve our tasting skills. Therefore the program will be a mix of aroma sessions, basic taste theory",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
    {
        "img_location": "../src/img/Nyhavn.png",
        "title": "Nordic Barista Cup 2011 in Helsinki\n",
        "date": "Posted: 22/1-2021",
        "text": "Nordic Barista Cup 2011 will be held in Copenhagen, Denmark.\n" +
            "Dates: 25th - 27th August 2011. The theme for the 2011 seminar is: SENSORY. More information will follow on this page.\n",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"

    },
    {
        "img_location": "../src/img/Winners.png",
        "title": "2210 Winners: Finland" ,
        "date": "Posted: 12/1-2021",
        "text": "Oh my goodness, the final night is here! We are at the most incredible location in all of Oslo—well, at least that is what I think, since I havent seen much of anything else around here.",
        "text_1": "Maecenas nisl est, ultrices nec congue eget, auctor vitae massa. Fusce luctus vestibulum augue ut aliquet"
    },
]

window.onload = function(){
    slideShow();
    loadArticles();
    pagination()
};
loadArticles = ()=>{
    for (let i = 0; i < articles.length; i = i +1 ){
        $( "#articles-wrap" ).append(
           `
        <div class="card-container px-4 py-3">
        <div class="card-wrap d-flex">
             <img 
            class="card-img-top card-image-article" 
            src=${articles[i].img_location} 
            alt="Card image cap"
            >
            <div class="card-body w-50 d-flex flex-column  ">
                <h5 class="card-title ">${articles[i].title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${articles[i].date}</h6>
                <p class="card-text">
                    ${articles[i].text}     
                    <span id="dots">...</span>
                    <span id="more"> ${articles[i].text_1} </span>
                 </p>
                <button 
                type="button" 
                onclick="readMore()"
                id="btn-article"
                class="btn btn-article w-25"
                >
                Read more
                </button>
            </div>
        </div>
        </div>
           `

        );
    }
}
pagination=()=>{
    $('#articles-wrap').after('<div id="pagination" class="d-flex justify-content-start ps-4 pt-2"></div>');
    let rowsShown = 3;
    let rowsTotal = $('#articles-wrap .card-container').length;
    let numPages = rowsTotal/rowsShown;

    for(i = 0;i < numPages;i++) {
        let pageNum = i + 1;
        $('#pagination').append('<a class="page-item" rel="'+i+'">'+pageNum+'</a> ');
    }
    $('#articles-wrap .card-container').hide();
    $('#articles-wrap .card-container').slice(0, rowsShown).show();
    $('#pagination a:first').addClass('active');
    $('#pagination a').bind('click', function(){

        $('#pagination a').removeClass('active');
        $(this).addClass('active');
        let currPage = $(this).attr('rel');
        let startItem = currPage * rowsShown;
        let endItem = startItem + rowsShown;
        $('#articles-wrap .card-container').css('opacity','0.0').hide().slice(startItem, endItem).
        css('display','flex').animate({opacity:1}, 300);
    });
}

slideShow =() => {
    let current = 0,
        imgs = jQuery('#carousel .carousel-item');
    imgAmount = imgs.length;
    $(imgs.css('position', 'absolute').hide().get(0)).show();
    window.setInterval(swapImages, 1000);
    function swapImages() {
        var currentImg = $('.carousel-item:visible');
        var nextImg = $('.carousel-item:hidden').eq(Math.floor(Math.random() * $('.carousel-item:hidden').length));
        speed = 500;
        // animation speed should be the same for both images so we have a smooth change
        currentImg.fadeOut(speed);
        nextImg.fadeIn(speed);
    }
}

getTitles = ()=> {
    let titles = []
    for (let i = 0; i < articles.length; i = i +1 ){
        titles.push(articles[i].title)
    }
    return titles
}
$( "#input-search" ).autocomplete({
    source: getTitles()
});
searchArticle = ()=> {
    let searchValue = document.getElementById('input-search');
    let articlesContainer = document.getElementById('articles-wrap');

    $('#articles-wrap').remove();
    for (let i = 0; i < articles.length; i = i +1 ){
        if (searchValue.value === articles[i].title){
            $( '#articles-container' ).prepend(`
        <div class=" px-4 py-3">
        <div class="card d-flex px-4">
        <img
        class="card-img-top card-image-article"
        src=${articles[i].img_location}
        alt="Card image cap"
        >
            <div class="card-body w-50 d-flex flex-column  ">
                <h5 class="card-title ">${articles[i].title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${articles[i].date}</h6>
                <p class="card-text">
                    ${articles[i].text}
                    <span id="dots">...</span>
                     <span id="more"> ${articles[i].text_1} </span>
                     </p>
                <button
                type="button"
                id="btn-article"
                class="btn btn-article w-25"
                >
                Read more
                </button>
            </div>
        </div>
        </div>
           `);
        }else{
            $('#articles-wrap').empty();
            loadArticles()
        }
    }
}

$(document).ready(function() {
    $(document).on("click", ".btn-article", function (event ) {
        event.preventDefault();
        let dots = $(this).siblings('.card-text').children()[0];
        let moreText = $(this).siblings('.card-text').children()[1];
        let btnText = $(this).parent().children()[3];

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    });
});
